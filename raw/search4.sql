-- $Id: search4.sql,v 1.3 2016/03/02 08:28:35 clemens Exp clemens $
-- .
-- the scheme-name is mandatory here!
-- replace it with a different name if needed.
-- (later, we can re-code that in a variable, but for ..)
-- .

DROP PROCEDURE IF EXISTS `shop4_07`.`sp_search4`;

DELIMITER $$
CREATE PROCEDURE `shop4_07`.`sp_search4` (
	IN sstr varchar(255)
)
BEGIN
	DECLARE tname varchar(255);
	DECLARE tdone BOOLEAN DEFAULT FALSE;
	DECLARE cdone BOOLEAN DEFAULT FALSE;

	DECLARE sqlmode varchar(255);

	DECLARE cTables CURSOR FOR
		SELECT
			`table_name`
		FROM
			`information_schema`.`tables`
		WHERE
			`table_schema` = "shop4_07"
    ;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET tdone := TRUE;
	/*DECLARE CONTINUE HANDLER FOR SQLSTATE '02000' SET tdone := 1;*/


	-- becasue there can be values with more than the allowed 21845 chars
	-- we store the current sql-mode and..
    SET @sqlmode := (SELECT @@sql_mode);
	-- ..change the sql-mode to let mysql truncate the too long columns
    SET session sql_mode='';

	CREATE TEMPORARY TABLE IF NOT EXISTS results(
		  id int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT
		, tab_name varchar(255)
		, col_name varchar(255)
		, content varchar(255)
	)
	ENGINE MEMORY
	;


	OPEN cTables;
	trepeat: REPEAT
		FETCH cTables into tname;

		-- if the handler hits, we leave this loop immediate!
		-- .
		IF tdone THEN
			LEAVE trepeat;
		END IF;


		-- for each column in this table we create a nested cursor and
		-- check each columns value
		-- .
		columnsblock: BEGIN
			DECLARE cname varchar(255);
			DECLARE cColumns CURSOR FOR         /* declare a nested cursor for each column in the current table */
				SELECT
					column_name
				FROM
					information_schema.columns
				WHERE
					table_schema = 'shop4_07'
					AND table_name = tname
				;
			DECLARE CONTINUE HANDLER FOR NOT FOUND SET cdone := TRUE;


			OPEN cColumns;
			crepeat: REPEAT                     /* use a repeat with label, to break in the loop via 'leave' (to prevent a doublicate last loop-run) */
				FETCH cColumns INTO cname;      /* get next column-name from cursor */

				-- if the handler hits, we leave this loop immediate!
				-- .
				IF cdone THEN
					LEAVE crepeat;
				END IF;

				-- create a temp-table for our found rows (or use the existing one)
				-- IMPORTANT: don't forget to remove it again, after the work is done!
				-- .
				CREATE TEMPORARY TABLE IF NOT EXISTS found_tab(
					-- fvalue varchar(255)    /* since server 5.0.3 the length of 65535 is allowed */
                    fvalue varchar(21845)  /* but the server tells the maximum, but to short anymore :( */
				) ENGINE MEMORY;


				-- let's do some magic here - do "dynamic SQL"
				-- (prepare a query with variable structural elements, like table-names are)
				-- .
				SET @findq := concat('INSERT INTO found_tab SELECT `',cname,'`  FROM `',tname,'` WHERE cast(`',cname,'` AS char) LIKE cast("',concat('%',sstr,'%'),'" AS char);');
				PREPARE stm1 FROM @findq;
				EXECUTE stm1;
				DEALLOCATE PREPARE stm1;          /* freeup our used handle again */

				-- save the found row(s) in the result-table
				-- .
				resultblock: BEGIN
				/* {{{ */
					DECLARE fresult varchar(255);
					DECLARE fdone BOOLEAN DEFAULT FALSE;
					DECLARE cFound CURSOR FOR SELECT fvalue FROM found_tab;
					DECLARE CONTINUE HANDLER FOR NOT FOUND SET fdone := TRUE;

					OPEN cFound;
					REPEAT
						FETCH cFound INTO fresult;

						IF fresult IS NOT NULL AND !fdone THEN
							INSERT INTO
								results(
									  tab_name
									, col_name
									, content
								)
							VALUES(
								  tname
								, cname
								, fresult
							   )
							;
						END IF;

					UNTIL fdone
					END REPEAT;

					SET fdone := FALSE;         /* don't forget to reset the "not-found"-handler-flag! */
					CLOSE cFound;
				/* }}} */
				END resultblock;

				TRUNCATE TABLE found_tab;         /* empty our found-tab again! */
			UNTIL cdone END REPEAT crepeat;       /* write a leave with label (see loop-start above) */

			-- cleanups for each loop-run
			-- .
			SET cdone := FALSE;        /* reset the handler-flag */
			CLOSE cColumns;            /* close the cursor */

		END columnsblock;


	UNTIL tdone END REPEAT trepeat;
	SET tdone := FALSE;
	CLOSE cTables;


	-- output all our results and
	-- cleanup (remove the temp-table again)
	-- .
	SELECT * FROM results;
	DROP TABLE results;            /* remove the result-tab too, after sent the result-set */
	DROP TABLE found_tab;          /* remove our found-tab again! */

	-- switch back to the normal sql-mode before running this proc
	-- (default in 5.7.x : 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION')
	SET SESSION sql_mode=@sqlmode;
END$$
DELIMITER ;

call sp_search4('foo');

select @@sql_mode;
