-- $Id: cleanup_redirects.sql,v 0.4 2020/02/25 14:18:27 clemens Exp clemens $
-- specify your db-scheme-name here
USE shop5_01;

-- removing any previously existing one
DROP PROCEDURE IF EXISTS `sp_clean_redirects`;

-- .
DELIMITER $$
CREATE PROCEDURE `sp_clean_redirects` ()
COMMENT '$Id: cleanup_redirects.sql,v 0.4 2020/02/25 14:18:27 clemens Exp clemens $'
BEGIN
    DECLARE oDone BOOLEAN DEFAULT FALSE;
    DECLARE rDone BOOLEAN DEFAULT FALSE;
    DECLARE fromURL varchar(1024);
    DECLARE occurrence varchar(1024);
    DECLARE referrerCount int(10);
    DECLARE cOccurrences CURSOR FOR
        SELECT *
            FROM (SELECT cFromUrl, count(DISTINCT(kRedirect)) AS occurrence FROM tredirect GROUP BY cFromUrl) AS a
            WHERE a.occurrence >1
        ;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET oDone := TRUE;

    OPEN cOccurrences;
    oRepeat: REPEAT
        FETCH cOccurrences INTO fromURL, occurrence;

        IF oDone THEN
            LEAVE oRepeat;
        END IF;

        -- get the count of different referrers from tredirectreferer
        -- .
        SET referrerCount := (SELECT count(*)
            FROM tredirectreferer
            WHERE kRedirect IN (
                SELECT kRedirect FROM tredirect WHERE cFromUrl = fromURL COLLATE utf8_unicode_ci AND cToUrl = ''
            )
        );

        -- collect all referrers
        -- .
        blockReferrer: BEGIN
            DECLARE lastid int(10);
            DECLARE refid int(10);
            DECLARE redid int(10);
            DECLARE cReferrers CURSOR FOR
                SELECT kRedirectReferer, kRedirect
                    FROM tredirectreferer
                    WHERE kRedirect IN (SELECT kRedirect FROM tredirect WHERE cFromUrl = fromURL COLLATE utf8_unicode_ci)
                ;
            DECLARE CONTINUE HANDLER FOR NOT FOUND SET rDone := TRUE;

            -- per find we store one summary row instead
            -- (consider the 'utf8_unicode_ci': because the sp is utf8_general_ci at all, but the DB is uft_unicode_ci)
            -- .
            INSERT INTO tredirect VALUES(0, fromURL, '', referrerCount, '');
            SET lastid := last_insert_id();

            OPEN cReferrers;
            eRepeat: REPEAT
                FETCH cReferrers INTO refid, redid;

                IF rDone THEN
                    LEAVE eRepeat;
                END IF;

                UPDATE tredirectreferer AS a, tredirect AS b
                    SET a.kRedirect = lastid
                    WHERE a.kRedirectReferer = refid AND (a.kRedirect = b.kRedirect AND b.cToUrl = '')
                ;

            UNTIL rDone END REPEAT eRepeat;
            SET rDone := FALSE;
            CLOSE cReferrers;

            -- we remove all occurences but not the new inserted one
            -- .
            DELETE FROM tredirect WHERE cFromUrl = fromURL COLLATE utf8_unicode_ci AND kRedirect != lastid AND cToUrl = '';

        END blockReferrer;

    UNTIL oDone END REPEAT oRepeat;
    SET oDone := FALSE;
    CLOSE cOccurrences;

END$$
DELIMITER ;

-- call the new created stored procedure
call sp_clean_redirects();

-- show the state of all stored procedures
show procedure status;


    /* (was inside the SP)
     * note: needed anymore (but: here for remind me later)
     *
     *SET NAMES utf8;
     *SET collation_connection=utf8_unicode_ci;
     *SET character_set_results=utf8;
     *SET character_set_client=utf8;
     *SET character_set_connection=utf8;
     */


-- fake data
insert into tredirect values(0,'/graubart', '', 0, ''),(0,'/graubart', '', 0, ''),(0,'/graubart', '', 0, ''),(0,'/graubart', '', 0, ''),(0,'/graubart', '', 0, '');
insert into tredirect values(0,'/graubartjacke', '', 0, ''),(0,'/graubartjacke', '', 0, ''),(0,'/graubartjacke', '', 0, ''),(0,'/graubartjacke', '', 0, ''),(0,'/graubartjacke', '', 0, ''),(0,'/graubartjacke', '', 0, '');
insert into tredirectreferer values
     (0,101,0,'','::1',1574757660)
    ,(0,106,0,'','localhost',1574757697)
    ,(0,107,0,'','123.1.2.3',1574757753)
    ,(0,110,0,'','34.23.45.6',1582549952)
    ,(0,103,0,'','78.56.34.2',1574757753)
    ,(0,101,0,'','78.56.34.2',1574757753)
    ,(0,110,0,'','34.23.45.6',1582549952)
    ,(0,103,0,'','78.56.34.2',1574757753)
    ,(0,101,0,'','78.56.34.2',1574757753)
    ,(0,107,0,'','123.1.245.3',1574757661)
    ,(0,107,0,'','123.1.2.36',1574757678)
;

delete from tredirect where kRedirect >99;
alter table tredirect auto_increment = 100;
delete from tredirectreferer where kRedirectReferer >=80;
alter table tredirectreferer auto_increment=80;
