# Stored Procedure to search strings in MySQL-schemes

ATTENTION: use it at your own risc!

This stored procedure search a DB-scheme for some given text
and display table and column, where it was found
(multiple times, if needed).
Every column-content is interpreted as a string, to enable this
"search over all fields".


USAGE:

First set the current scheme-name inside the stored procedure!

`$> vi search4xxx.sql` (... and edit the scheme-name!)

Than you have to execute this SQL against your DB-scheme, like:
(xxx = script-Version, yyy = Username)

`$> mysql -u yyy -p my_scheme < search4xxx.sql`

After that, you can use it this way (e.g. in the mysql-cli):

```sql
call sp_search4('arbitrary string');
call sp_search4('.%.%.');  -- e.g. for an IP
```

NOTE:  
The search-string is enclosed by '%' in a prepared statement,  
inside the stored procedure. This makes it possible, to do  
somthing like the IP-search-example above.
