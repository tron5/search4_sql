
-- $Id: search4tc.sql,v 0.1 2018/06/27 11:50:30 clemens Exp clemens $
-- .
-- ATTENTION:
-- the scheme-name is mandatory here!
-- replace it with your current database-/scheme-name.
-- .


-- specify your db-scheme-name here
USE shop4_07;

-- removing any previously existing one
DROP PROCEDURE IF EXISTS `sp_search4tc`;

-- create a new one
DELIMITER $$
CREATE PROCEDURE `sp_search4tc` (
	IN sstr varchar(255)
)
COMMENT '$Id: search4tc.sql,v 0.1 2018/06/27 11:50:30 clemens Exp clemens $'
BEGIN
	DECLARE tname varchar(512);
	DECLARE tdone BOOLEAN DEFAULT FALSE;
	DECLARE cdone BOOLEAN DEFAULT FALSE;
	DECLARE cname varchar(512);
	DECLARE tablename varchar(512);

	DECLARE cTables CURSOR FOR
		SELECT
			`table_name`
		FROM
			`information_schema`.`tables`
		WHERE
			`table_schema` = @dbname
			AND `table_name` like concat('%',sstr,'%')
	;
	DECLARE cColumns CURSOR FOR         /* declare a nested cursor for each column in the current table */
		SELECT
			column_name
			, table_name
		FROM
			information_schema.columns
		WHERE
			table_schema = @dbname
			AND table_name = tname
			AND column_name like concat('%',sstr,'%')
		;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET tdone := TRUE;
	/*DECLARE CONTINUE HANDLER FOR NOT FOUND SET cdone := TRUE;*/


	SET @dbname := (SELECT database());


	CREATE TEMPORARY TABLE IF NOT EXISTS results(
		  id BIGINT PRIMARY KEY NOT NULL AUTO_INCREMENT
		, tab_name varchar(512)
		, col_name varchar(4096)
		, content  varchar(512)
	)
	ENGINE=MEMORY
	ROW_FORMAT=DYNAMIC
	KEY_BLOCK_SIZE=8
	CHARACTER SET utf8 COLLATE utf8_general_ci
	;


	OPEN cTables;
	twhile: WHILE tdone != TRUE DO
		FETCH cTables INTO tname;

		-- if the handler hits, we leave this loop immediate!
		-- .
		IF tdone THEN
			LEAVE twhile;
		END IF;


		INSERT INTO results(tab_name, col_name) VALUES(tname, @cnames);

	END WHILE twhile;
	CLOSE cTables;

	set tdone := FALSE;
	SET SQL_SAFE_UPDATES = FALSE;

	OPEN cColumns;
	cwhile: WHILE tdone != TRUE DO
		FETCH cColumns INTO cname,tablename;

		IF tdone THEN
			LEAVE cwhile;
		END IF;

		/*SET @cnames := (SELECT concat(@cnames, cname));*/
		update ignore results set col_name = cname where tab_name = tablename;

	END WHILE cwhile;
	CLOSE cColumns;




	select * from results; /* --DEBUG-- */
	drop table results;

END$$
DELIMITER ;

-- test the new created one, if needed (and by hand!)
call sp_search4tc('cName');
call sp_search4tc('kunde');



-- tables
SELECT * FROM information_schema.tables WHERE table_name LIKE '%kunde%' AND table_schema = 'shop4_07';
-- columns
SELECT table_name, concat(column_name,' (',column_type,')') FROM information_schema.columns WHERE column_name LIKE '%Name%' AND TABLE_SCHEMA = 'shop4_07' ORDER BY table_name;
SELECT table_name, concat(column_name,' (',column_type,', ',CHARACTER_SET_name,')') AS type_info FROM information_schema.columns WHERE column_name LIKE '%Name%' AND TABLE_SCHEMA = 'shop4_07' ORDER BY table_name;
SELECT * FROM information_schema.columns WHERE column_name LIKE '%Name%' AND TABLE_SCHEMA = 'shop4_07' ORDER BY table_name;


